

from tkinter.simpledialog import askstring
import os
import json

def normalize_coordinates(x, y, width, height):
    normalized_x = x / width
    normalized_y = y / height
    return normalized_x, normalized_y

def savetemplate(template_data, space_width, space_height):
    filename = askstring("Save Template", "Enter a filename:")
    if filename:
        data_folder = "data"
        os.makedirs(data_folder, exist_ok=True)
        
        keypoints = template_data["keypoints"]
        lines = template_data["lines"]
        
        # Calculate bounding box based on the keypoints
        min_x = min(keypoints, key=lambda p: p[0])[0]
        min_y = min(keypoints, key=lambda p: p[1])[1]
        max_x = max(keypoints, key=lambda p: p[0])[0]
        max_y = max(keypoints, key=lambda p: p[1])[1]

        # Normalize bounding box coordinates
        normalized_min_x, normalized_min_y = normalize_coordinates(min_x, min_y, space_width, space_height)
        normalized_max_x, normalized_max_y = normalize_coordinates(max_x, max_y, space_width, space_height)

        bounding_box = [normalized_min_x, normalized_min_y, normalized_max_x, normalized_max_y]

        # Normalize key points coordinates
        save_data = []
        for x, y in keypoints:
            normalized_x, normalized_y = normalize_coordinates(x, y, space_width, space_height)
            key_points = [normalized_x, normalized_y]

        # Include label outside the loop
        save_data.append({
            "label": 1,
            "bounding_box": bounding_box,
            "key_points": key_points
        })

        with open(os.path.join(data_folder, f"{filename}.json"), "w") as json_file:
            json.dump(save_data, json_file, indent=4)
        
        print(f"Template saved to {filename}.json")

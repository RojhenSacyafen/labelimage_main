
from Keypoint_files.LoadDrawKeypointsAndLines import drawkeypointsand_linesload
from Keypoint_files.ClearTemplate import cleartemplate
from tkinter.simpledialog import askstring
import json
import os
from Keypoint_files.modemanager import mode_manager

def loadtemplate(canvas,xlabel,ylabel,zlabel,template_data,checkbox_list):
    try:
        
        filename = askstring("Load Template", "Enter a filename:")
        cleartemplate(canvas,xlabel,ylabel,zlabel,template_data,checkbox_list)
        with open(os.path.join("data/data1", f"{filename}.json"), "r") as json_file:
             loaded_data = json.load(json_file)
        
        template_data["keypoints"] = loaded_data.get("keypoints", [])
        template_data["lines"] = loaded_data.get("lines", [])

        drawkeypointsand_linesload(canvas,template_data)

         # Extract and display the coordinates
        coordinates = loaded_data.get("coordinates", [])
        if coordinates:
            last_coordinate = coordinates[-1]
            xlabel.configure(text=str(last_coordinate["x"]))
            ylabel.configure(text=str(last_coordinate["y"]))
            zlabel.configure(text=str(last_coordinate["z"]))
        print(f"Template loaded from {filename}.json")
    except FileNotFoundError:
        print("Template file not found. Please save a template first.")

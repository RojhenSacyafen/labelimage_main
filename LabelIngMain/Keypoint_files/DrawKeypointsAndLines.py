

from Keypoint_files.DrawGrid import draw_grids1
from Keypoint_files.ConnectKeypoints import connectkeypoints


def draw_keypoints_and_linesss1(canvas, template_data):
    canvas.delete("Keypoints", "lines", "labels")
    draw_grids1(canvas)

    for i, (x, y) in enumerate(template_data["keypoints"]):
        canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="blue", fill="red")
        label_text = f"Keypoint {i}"
        canvas.create_text(x, y - 10, text=label_text, tags="labels")

    # if len(template_data["keypoints"]) > 1:
    #     for i in range(len(template_data["keypoints"]) - 1):
    #         x1, y1 = template_data["keypoints"][i]
    #         x2, y2 = template_data["keypoints"][i + 1]
    #         canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
            
    # connectkeypoints(canvas, template_data)
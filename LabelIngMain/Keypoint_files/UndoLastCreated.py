
from Keypoint_files.DrawKeypointsAndLines import draw_keypoints_and_linesss1


def undo_lastkeypoint(canvas,template_data,checkbox_list):
    if len(template_data["keypoints"]) > 0:
        template_data["keypoints"].pop()
    
        template_data["lines"] = []
        draw_keypoints_and_linesss1(canvas,template_data)
        # Remove the last checkbox item
        if checkbox_list:
            checkbox_list[-1].destroy()
            checkbox_list.pop()
        print("Undone")
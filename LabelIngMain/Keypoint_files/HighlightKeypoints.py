

def highlight_keypoint(canvas, selected_keypoint):
    x, y = selected_keypoint
    canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="blue", fill="blue")

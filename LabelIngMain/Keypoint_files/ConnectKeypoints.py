
from Keypoint_files.globalMode import modes
from Keypoint_files.modemanager import mode_manager
def connectkeypoints(canvas,template_data):
        keypoints = template_data["keypoints"]
        global modes
        if mode_manager.mode =="start":
            lines = []
            for i in range(len(keypoints) - 1):
                x1, y1 = keypoints[i]
                x2, y2 = keypoints[i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
                lines.append([(x1, y1), (x2, y2)])
            template_data["lines"] = lines
            print("keypoints connected")
            

from Keypoint_files.DrawKeypointsAndLines import draw_keypoints_and_linesss1
from Keypoint_files.DrawGrid import draw_grids1
from tkinter.simpledialog import askstring
import os
import json
from Keypoint_files.globalModeFinished import modes
from Keypoint_files.modemanager import mode_manager



def on_keypoint_dragged(event, canvas,template_data, keypoint_manager):
    if mode_manager.mode == "finish":
        drag_data = keypoint_manager.drag_data
        if drag_data:
            delta_x = event.x - drag_data["x"]
            delta_y = event.y - drag_data["y"]
            # Check for collision with other keypoints
            new_x = drag_data["x"] + delta_x
            new_y = drag_data["y"] + delta_y
            collision = False
            for x, y in template_data["keypoints"]:
                if (new_x - x)**2 + (new_y - y)**2 < 1:  # Adjust this value of Collisionn
                    collision = True
                    break
            if not collision:
                for i, (x, y) in enumerate(template_data["keypoints"]):
                    if (x, y) == (drag_data["x"], drag_data["y"]):
                        template_data["keypoints"][i] = (x + delta_x, y + delta_y)
                      
                for i, line in enumerate(template_data["lines"]):
                    for j, (x, y) in enumerate(line):
                        if (x, y) == (drag_data["x"], drag_data["y"]):
                            template_data["lines"][i][j] = (x + delta_x, y + delta_y)
                
                draw_keypoints_and_lines_loads1(canvas,template_data)
                drag_data["x"] = event.x
                drag_data["y"] = event.y
    elif mode_manager.mode == "start":
        print("Nothing")
def draw_keypoints_and_lines_loads1(canvas,template_data):
    canvas.delete("all")
    draw_grids1(canvas)
    for i, (x, y) in enumerate(template_data["keypoints"]):
        canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="blue", fill="red",tags="keypoints")
        label_text = f"Keypoint {i}"
        canvas.create_text(x, y - 10, text=label_text, tags="labels")
        
    for line in template_data["lines"]:
        x1, y1 = line[0]
        x2, y2 = line[1]
        canvas.create_line(x1, y1, x2, y2, fill="blue", width=2, tags="lines")
 





class ModeManager:
    def __init__(self):
        self.mode = None
    def set_mode(self, mode):
        self.mode = mode
mode_manager = ModeManager()

def startmode(canvas, xlabel, ylabel, zlabel, template_data, checkbox_list):
    global modes
    modes = []
    modes = ""
    modes = "start"
    canvas.delete("Keypoints", "lines")
  
    if not template_data["keypoints"]:
        template_data["keypoints"] = []
        template_data["keypoint_counter"] = 0
    # template_data["keypoints"] = []
    # Reset coordinate labels
    xlabel.configure(text="0000")
    ylabel.configure(text="0000")
    zlabel.configure(text="0000")
    print("Click Start")
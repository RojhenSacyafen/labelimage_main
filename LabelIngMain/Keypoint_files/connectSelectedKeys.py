import random

def connect_selected_keypoints(canvas, template_data):
    selected_keypoints = template_data["selected_keypoints"]
    
    for i in range(len(selected_keypoints) - 1):
        x1, y1 = selected_keypoints[i]
        x2, y2 = selected_keypoints[i + 1]
        canvas.create_line(x1, y1, x2, y2, fill="red", width=2)
        template_data["lines"].append([(x1, y1), (x2, y2)])
        # template_data["lines"] = lines
    template_data["selected_keypoints"]=[]
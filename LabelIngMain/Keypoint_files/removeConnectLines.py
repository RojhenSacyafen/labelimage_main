from Keypoint_files.DrawGrid import draw_grids1


def remove_or_connect_button_click(canvas, template_data):
    selected_keypoints = template_data["lines"]
    
    for line_id in selected_keypoints:
        canvas.delete(line_id)
        print("Line Deleted")
    drawkeypointsand_linesload(canvas,template_data)
    # Reset the lines list
    template_data["selected_keypoints"] = []

def drawkeypointsand_linesload(canvas,template_data):
        canvas.delete("keypoints","lines")
        draw_grids1(canvas)
        for i, (x, y) in enumerate(template_data["keypoints"]):
            canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="blue", fill="red")
            label_text = f"Keypoint {i}"
            canvas.create_text(x, y - 10, text=label_text, tags="labels")
        for line in template_data["lines"]:
            x1, y1 = line[0]
            x2, y2 = line[1]
            canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
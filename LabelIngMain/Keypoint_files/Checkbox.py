import customtkinter

checkbox_list=[]

def checkbox_frame_event():
    print(f"checkbox frame modified: {get_checked_items()}")

def add_item(scrollable_frame,item):
    checkbox = customtkinter.CTkCheckBox(scrollable_frame, text=item, command=checkbox_frame_event)
    checkbox.grid(row=len(checkbox_list), column=0, pady=(0, 10))
    checkbox_list.append(checkbox)

def remove_item(item):
    for checkbox in checkbox_list:
        if item == checkbox.cget("text"):
            checkbox.destroy()
            checkbox_list.remove(checkbox)
            return
def get_checked_items():
    return [checkbox.cget("text") for checkbox in checkbox_list if checkbox.get() == 1]
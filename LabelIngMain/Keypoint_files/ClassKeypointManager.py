

from Keypoint_files.DrawKeypointsAndLines import draw_keypoints_and_linesss1
from tkinter import messagebox
class KeypointManager:
    def __init__(self):
        self.template_data = {"keypoints": [], "lines": [], "keypoint_counter":0, "selected_keypoints": []}
        self.drag_data = {"x": 0, "y": 0}
        self.undo_stack = []  
        self.max_undo_steps = 10 
        self.keypoints = []
        self.selected_keypoint_index = None
        self.select_key = {"x": 0, "y": 0}
    def undo_keypoint(self, canvas):
        if len(self.undo_stack) > 0:
            # Pop the last state from the undo stack
            last_state = self.undo_stack.pop()
            # Update the existing template data
            self.template_data["keypoints"] = last_state["keypoints"]
            self.clear_selected_keypoint()
            # Redraw keypoints and lines
            draw_keypoints_and_linesss1(canvas, self.template_data)
        else:
            messagebox.showinfo("Undo", "No more actions to undo.")
    def set_selected_keypoint(self, index):
        self.selected_keypoint_index = index
    def clear_selected_keypoint(self):
        self.selected_keypoint_index = None
    
    def select_keypoint(self, canvas, x, y):
        # Find the index of the keypoint at the given coordinates (x, y)
        for i, keypoint in enumerate(self.template_data["keypoints"]):
            if abs(keypoint["x"] - x) < 5 and abs(keypoint["y"] - y) < 5:
                self.set_selected_keypoint(i)
                messagebox.showinfo("KeyPoint Selected", f"Selected Keypoint at index {i}")
                return

        self.clear_selected_keypoint()
keypoint_manager = KeypointManager()

from Keypoint_files.DrawGrid import draw_grids1
def cleartemplate(canvas,xlabel,ylabel,zlabel,template_data,checkbox_list):
    template_data["keypoints"] = []
    template_data["lines"] = []
    canvas.delete("all")
    draw_grids1(canvas)
    xlabel.configure(text="0000")
    ylabel.configure(text="0000")
    zlabel.configure(text="0000")
    for checkbox in checkbox_list:
        checkbox.destroy()
    checkbox_list.clear()
    print("Cleared")
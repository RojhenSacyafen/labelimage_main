

def connectkeypoints_all(canvas,template_data):
        global connect_lines
        keypoints = template_data["keypoints"]
        if len(keypoints) > 1:
            lines = []
            for i in range(len(keypoints) - 1):
                x1, y1 = keypoints[i]
                x2, y2 = keypoints[i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
                lines.append([(x1, y1), (x2, y2)])
            # Connect the last and first keypoints
            x1, y1 = keypoints[-1]
            x2, y2 = keypoints[0]
            canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
            lines.append([(x1, y1), (x2, y2)])
            template_data["lines"] = lines
            print("1st and Last Keypoint Connected")
connect_lines = True
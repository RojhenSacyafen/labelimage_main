
from Keypoint_files.DrawKeypointsAndLines import draw_keypoints_and_linesss1
from Keypoint_files.Checkbox import add_item
from Keypoint_files.DrawGrid import draw_grids1
from Keypoint_files.globalMode import modes
from Keypoint_files.modemanager import mode_manager
from Keypoint_files.HighlightKeypoints import highlight_keypoint
from tkinter import messagebox
from Keypoint_files.ClassKeypointManager import keypoint_manager

def undo_keypoint_action(canvas):
    keypoint_manager.undo_keypoint(canvas)

def on_clickas1(event, canvas, xlabel, ylabel, zlabel, scrollable_frame, template_data):
    x, y = event.x, event.y
    nearest_keypoint = find_nearest_keypoint(x, y,event, template_data)
    if mode_manager.mode == "start":
        template_data["keypoints"].append((x, y))
        draw_keypoints_and_linesss1(canvas, template_data)

        xlabel.configure(text=str(x))
        ylabel.configure(text=str(y))
        z = len(template_data["keypoints"])
        zlabel.configure(text=str(z))

        checkbox_text = f"Keypoint {template_data['keypoint_counter']}"
        add_item(scrollable_frame, checkbox_text)

        # Increment the keypoint counter
        template_data["keypoint_counter"] += 1
    elif mode_manager.mode == "finish":
        
        # Find all keypoints within a certain radius
        radius = 10  # Adjust this value as needed
        nearby_keypoints = [(x, y) for x, y in template_data["keypoints"]
                    if (x - event.x)**2 + (y - event.y)**2 <= radius**2]
        # If there are nearby keypoints, choose the one with the highest y value
        if nearby_keypoints:
            nearest_keypoint = max(nearby_keypoints, key=lambda point: point[1])
            keypoint_manager.drag_data["x"] = nearest_keypoint[0]
            keypoint_manager.drag_data["y"] = nearest_keypoint[1]
        xlabel.configure(text=str(x))
        ylabel.configure(text=str(y))
        z = len(template_data["keypoints"])
        zlabel.configure(text=str(z))
    # elif mode_manager.mode == "connect":
    #     x, y = event.x, event.y
    #     selected_keypoint = find_nearest_keypoint(x, y,event, template_data)
    #     if selected_keypoint:
    #         highlight_keypoint(canvas, selected_keypoint) 
    #     for i, (key_x, key_y) in enumerate(template_data["keypoints"]):
    #         distance = ((x - key_x) ** 2 + (y - key_y) ** 2) ** 0.5
    #         if distance < 10:  # Adjust the radius for clicking sensitivity
    #             template_data["selected_index"] = i
    #             break  
    elif mode_manager.mode == "connect":
        x, y = event.x, event.y
        selected_keypoint = find_nearest_keypoint(x, y, event, template_data)
        if selected_keypoint:
            template_data["selected_keypoints"].append(selected_keypoint)
            highlight_keypoint(canvas, selected_keypoint)

def find_nearest_keypoint(x, y,event, template_data):
    radius = 10
    nearby_keypoints = [(x, y) for x, y in template_data["keypoints"]
                        if (x - event.x)**2 + (y - event.y)**2 <= radius**2]

    if nearby_keypoints:
        nearest_keypoint = max(nearby_keypoints, key=lambda point: point[1])
        return nearest_keypoint
    else:
        return None

def startmode(canvas, xlabel, ylabel, zlabel, template_data, checkbox_list):
    global modes
    modes = []
    modes = ""
    modes = "start"
    canvas.delete("Keypoints", "lines")
    # Reset only if it's the first set
    if not template_data["keypoints"]:
        template_data["keypoints"] = []
        template_data["keypoint_counter"] = 0
    # template_data["keypoints"] = []
    # Reset coordinate labels
    xlabel.configure(text="0000")
    ylabel.configure(text="0000")
    zlabel.configure(text="0000")
    print("Click Start")



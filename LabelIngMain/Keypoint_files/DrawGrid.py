

GRID_SIZE=25
def draw_grids1(canvas):
        for x in range(0, canvas.winfo_reqwidth(), GRID_SIZE):
            canvas.create_line(x, 0, x, canvas.winfo_reqheight(), fill="gray", dash=(2, 2))
        for y in range(0, canvas.winfo_reqheight(), GRID_SIZE):
            canvas.create_line(0, y, canvas.winfo_reqwidth(), y, fill="gray", dash=(2, 2))
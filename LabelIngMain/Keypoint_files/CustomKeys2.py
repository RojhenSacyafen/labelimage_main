
import customtkinter as tk
import customtkinter
import json
from tkinter.simpledialog import askstring
import os
from Keypoint_files.Checkbox import add_item, remove_item
from Keypoint_files.ClearTemplate import cleartemplate
from Keypoint_files.ConnectFirstLastKeypoints import connectkeypoints_all
from Keypoint_files.ConnectKeypoints import connectkeypoints
from Keypoint_files.DragKeypoints import on_keypoint_dragged
from Keypoint_files.DrawGrid import draw_grids1
from Keypoint_files.DrawKeypointsAndLines import draw_keypoints_and_linesss1
from Keypoint_files.LoadDrawKeypointsAndLines import drawkeypointsand_linesload
from Keypoint_files.LoadTemplate import loadtemplate
from Keypoint_files.OnClickStartFinish import on_clickas1,startmode,undo_keypoint_action
from Keypoint_files.ClassKeypointManager import keypoint_manager
from Keypoint_files.SaveTemplate import savetemplate, savetemplate2
from Keypoint_files.connectSelectedKeys import connect_selected_keypoints
from Keypoint_files.removeConnectLines import remove_or_connect_button_click
# from files.UndoLastCreated import undo_lastkeypoint
from Keypoint_files.modemanager import mode_manager

def CusKey():
    customtkinter.set_appearance_mode("System")
    customtkinter.set_default_color_theme("blue")
    root = customtkinter.CTk()
    root.geometry("920x560")
    root.title("Keypoints Labeling")

    canvas = tk.CTkCanvas(root, width=560, height=480, bg="white",highlightbackground="black", highlightthickness=1)
    canvas.place(x=5,y=5)

    template_data = keypoint_manager.template_data

    box1 = tk.CTkFrame(root, width=300, height=150)
    box1.place(x=600,y=5)
    tittleLabel = tk.CTkLabel(box1, width=280, height=25,text="Coordinates", bg_color="transparent", fg_color="green",text_color="white", corner_radius=10)
    tittleLabel.place(x=10,y=10)

    Kypoint_Label = tk.CTkLabel(box1,text="Keypoint: ")
    Kypoint_Label.place(x=20,y=40)
    Kypoint_Number = tk.CTkLabel(box1,text="00 ")
    Kypoint_Number.place(x=90,y=40)

    xCoordinate = tk.CTkLabel(box1,text="X: ")
    xCoordinate.place(x=100,y=60)
    xlabel = tk.CTkLabel(box1,text="0000")
    xlabel.place(x=140,y=60)

    yCoordinate = tk.CTkLabel(box1,text="Y: ")
    yCoordinate.place(x=100,y=90)
    ylabel = tk.CTkLabel(box1,text="0000")
    ylabel.place(x=140,y=90)

    zCoordinate = tk.CTkLabel(box1,text="Z: ")
    zCoordinate.place(x=100,y=120)
    zlabel = tk.CTkLabel(box1,text="0000")
    zlabel.place(x=140,y=120)

    box2 = tk.CTkFrame(root, width=300, height=370)
    box2.place(x=600,y=170)


    box2_label_names = tk.CTkLabel(box2, width=280, height=25, text="Keypoints", bg_color="transparent", fg_color="green", text_color="white", corner_radius=10)
    box2_label_names.grid(row=0, column=0, pady=(5, 10), padx=15, sticky="ns")

    # create scrollable checkbox frame
    scrollable_frame = customtkinter.CTkScrollableFrame(box2, width=200)
    scrollable_frame.grid(row=1, column=0, padx=15, pady=15, sticky="ns")


    ##############################################################################################################################################################################3
    start_button = tk.CTkButton(root, text="Start", command=lambda: mode_manager.set_mode("start"))
    start_button.place(x=10,y=490)

    # connect_lines = tk.CTkButton(root, text="Connect",command=lambda: connect_selected_keypoints(canvas, template_data, template_data["selected_keypoints"], template_data["selected_keypoints"] - 1))
    connect_lines = tk.CTkButton(root, text="Connect", command=lambda: connect_selected_keypoints(canvas, template_data))
    connect_lines.place(x=610,y=490)
    remove_lines = tk.CTkButton(root, text="Remove Line", command=lambda: remove_or_connect_button_click(canvas, template_data))
    remove_lines.place(x=760,y=490)
    Select_keypoint = tk.CTkButton(root, text="Select_Kepoint", command=lambda: mode_manager.set_mode("connect"))
    Select_keypoint.place(x=610,y=525)

    finish_button = tk.CTkButton(root, text="Finish", command=lambda: mode_manager.set_mode("asd"))
    finish_button.place(x=10,y=525)
    # finish_button.bind("<Button-1>", finishmode)
    connect_button = tk.CTkButton(root, text="Connect", command=lambda: connectkeypoints_all(canvas,template_data))
    connect_button.place(x=160,y=490)

    undo_button = tk.CTkButton(root, text="Undo", command=lambda: undo_keypoint_action(canvas))
    undo_button.place(x=160,y=525)

    edit_button = tk.CTkButton(root, text="Edit", command=lambda: mode_manager.set_mode("finish"))
    edit_button.place(x=310,y=490)
    checkbox_list=[]
    clear_button = tk.CTkButton(root, text="Clear", command=lambda: cleartemplate(canvas,xlabel,ylabel,zlabel,template_data,checkbox_list))
    clear_button.place(x=310,y=525)

    save_button = tk.CTkButton(root, text="Save", command=lambda: savetemplate(template_data, space_width=560, space_height=480))
    save_button.place(x=460,y=490)

    load_button = tk.CTkButton(root, text="Load", command=lambda: [loadtemplate(canvas,xlabel,ylabel,zlabel,template_data,checkbox_list),mode_manager.set_mode("finish")])
    load_button.place(x=460,y=525)

    canvas.bind("<Button-1>", lambda event: on_clickas1(event, canvas, xlabel, ylabel, zlabel, scrollable_frame, template_data))

    canvas.bind("<B1-Motion>", lambda event: on_keypoint_dragged(event, canvas,template_data, keypoint_manager))


    draw_grids1(canvas)
    root.mainloop()
# CusKey()

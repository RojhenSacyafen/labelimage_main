

def show_frame(frame_to_show):
    for frame in [box_frame2_inside1, box_frame2_inside2, box_frame2_inside3]:
        if frame == frame_to_show:
            frame.grid(row=5, column=1, columnspan=2, padx=5, pady=5, sticky="nsew")
            frame.pack_propagate(False)
        else:
            frame.grid_remove()

def on_option_selected(value):
    if value == "Annotate":
        show_frame(box_frame2_inside1)
    elif value == "Keypoints":
        show_frame(box_frame2_inside2)
    elif value == "sample":
        show_frame(box_frame2_inside3)
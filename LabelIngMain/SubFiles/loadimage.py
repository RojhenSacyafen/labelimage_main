from tkinter.simpledialog import askstring
from PIL import Image, ImageTk
from tkinter import filedialog
import os
from SubFiles.ClassPath import data_folder
data_folder=data_folder.data_folders

def open_and_load_images1(box_frame2):
    global image_path, box_frame1_width,image

    image_path = filedialog.askopenfilename(filetypes=[("Image files", "*.png;*.jpg;*.jpeg;*.gif")])

    if image_path:
        image = Image.open(image_path)
        desired_height = 500
        aspect_ratio = image.width / image.height
        box_frame1_width = int(desired_height * aspect_ratio)
        image = image.resize((box_frame1_width, desired_height))

        # Clear existing widgets on box_frame2_canvas
        for widget in box_frame2.winfo_children():
            widget.destroy()

        tk_photo = ImageTk.PhotoImage(image)

        # Calculate the coordinates to center the image
        x_coordinate = (box_frame2.winfo_width() - image.width) // 2
        y_coordinate = (box_frame2.winfo_height() - image.height) // 2

        # Create and pack the image on box_frame2_canvas
        box_frame2.create_image(x_coordinate, y_coordinate, anchor="nw", image=tk_photo)
        box_frame2.image = tk_photo
def count_files_in_folder(folder_path):
    if not os.path.exists(folder_path) or not os.path.isdir(folder_path):
        return 0
    return len(os.listdir(folder_path))

number_of_files = count_files_in_folder(data_folder)
def save_loaded_image():
    global number_of_files
    if hasattr(image, 'save'):
        save_path = os.path.join(data_folder, f"image_{number_of_files}.png")
        if save_path:
            image.save(save_path)
            number_of_files += 1
            print(f"Image saved to {save_path}")
    else:
        print("No image loaded.")
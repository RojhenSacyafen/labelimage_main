import customtkinter as tk
import customtkinter
import json
from tkinter.simpledialog import askstring
import os
from tkinter import messagebox
GRID_SIZE = 25
cuskey_opened = False
def CustomKeypoints1():
    global cuskey_opened
    if not cuskey_opened:
        cuskey_opened = True
        CustomKeypoints11()
    else:
        messagebox.showinfo("Info", "cuskey is already open.")
def CustomKeypoints11():
    root = tk.CTk()
    def draw_grids1():
        for x in range(0, canvas.winfo_reqwidth(), GRID_SIZE):
            canvas.create_line(x, 0, x, canvas.winfo_reqheight(), fill="gray", dash=(2, 2))
        for y in range(0, canvas.winfo_reqheight(), GRID_SIZE):
            canvas.create_line(0, y, canvas.winfo_reqwidth(), y, fill="gray", dash=(2, 2))

    def on_clickas1(event):
        if mode == "start":
            template_data["keypoints"].append((event.x, event.y))
            draw_keypoints_and_linesss1()

    def draw_keypoints_and_linesss1():
        canvas.delete("all")
        draw_grids1()
        for x, y in template_data["keypoints"]:
            canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="red", fill="red")
        if len(template_data["keypoints"]) > 1:
            for i in range(len(template_data["keypoints"]) - 1):
                x1, y1 = template_data["keypoints"][i]
                x2, y2 = template_data["keypoints"][i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
        connectkeypoints()

    def drawkeypointsand_linesload():
        canvas.delete("all")
        draw_grids1()
        for x, y in template_data["keypoints"]:
            canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="red", fill="red")
        for line in template_data["lines"]:
            x1, y1 = line[0]
            x2, y2 = line[1]
            canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)

    def connectkeypoints():
        keypoints = template_data["keypoints"]
        if len(keypoints) > 1:
            lines = []
            for i in range(len(keypoints) - 1):
                x1, y1 = keypoints[i]
                x2, y2 = keypoints[i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
                lines.append([(x1, y1), (x2, y2)])
            template_data["lines"] = lines

    def connectkeypoints_all():
        keypoints = template_data["keypoints"]
        if len(keypoints) > 1:
            lines = []
            for i in range(len(keypoints) - 1):
                x1, y1 = keypoints[i]
                x2, y2 = keypoints[i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
                lines.append([(x1, y1), (x2, y2)])
            # Connect the last and first keypoints
            x1, y1 = keypoints[-1]
            x2, y2 = keypoints[0]
            canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
            lines.append([(x1, y1), (x2, y2)])
            template_data["lines"] = lines

    def undo_lastkeypoint():
        if len(template_data["keypoints"]) > 0:
            template_data["keypoints"].pop()
            draw_keypoints_and_linesss1()

    def startmode():
        global mode
        mode = "start"

    def finishmode():
        global mode
        mode = "finish"

    def cleartemplate():
        template_data["keypoints"] = []
        template_data["lines"]
        canvas.delete("all")

    def savetemplate():
        filename = askstring("Save Template", "Enter a filename:")
        if filename:
            data_folder = "data"
            os.makedirs(data_folder, exist_ok=True)
            with open(os.path.join(data_folder, f"{filename}.json"), "w") as json_file:
                json.dump(template_data, json_file)
            print(f"Template saved to {filename}.json")

    def loadtemplate():
        try:
            filename = askstring("Load Template", "Enter a filename:")
            with open(os.path.join("data", f"{filename}.json"), "r") as json_file:
                loaded_data = json.load(json_file)
            template_data["keypoints"] = loaded_data.get("keypoints", [])
            template_data["lines"] = loaded_data.get("lines", [])

            drawkeypointsand_linesload()
            print(f"Template loaded from {filename}.json")
        except FileNotFoundError:
            print("Template file not found. Please save a template first.")

    root = customtkinter.CTk()

    canvas = tk.CTkCanvas(root, width=560, height=480, bg="white")
    canvas.pack(expand=tk.YES, fill=tk.BOTH)

    template_data = {"keypoints": [], "lines": []}
    mode = "finish"

    start_button = tk.CTkButton(root, text="Start", command=startmode)
    start_button.pack(side=tk.LEFT, padx=10)
    finish_button = tk.CTkButton(root, text="Finish", command=finishmode)
    finish_button.pack(side=tk.LEFT)

    connect_button = tk.CTkButton(root, text="Connect", command=connectkeypoints_all)
    connect_button.pack(side=tk.LEFT, padx=10)

    undo_button = tk.CTkButton(root, text="Undo", command=undo_lastkeypoint)
    undo_button.pack(side=tk.LEFT, padx=10)

    clear_button = tk.CTkButton(root, text="Clear", command=cleartemplate)
    clear_button.pack(side=tk.LEFT, padx=10)
    save_button = tk.CTkButton(root, text="Save", command=savetemplate)
    save_button.pack(side=tk.LEFT)
    load_button = tk.CTkButton(root, text="Load", command=loadtemplate)
    load_button.pack(side=tk.LEFT)

    canvas.bind("<Button-1>", on_clickas1)
    draw_grids1()
    root.mainloop()
CustomKeypoints11()

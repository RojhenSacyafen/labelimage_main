import os
from tkinter.simpledialog import askstring
import json
import pdb
from SubFiles.ClassKeypointManager import keypoint_manager  

def load_templates1(box_frame2, template_data, image_width, image_height):
    try:
        filename = askstring("Load Template", "Enter a filename:")
        with open(os.path.join("data/data1", f"{filename}.json"), "r") as json_file:
             loaded_data = json.load(json_file)
        
        template_data["keypoints"] = loaded_data.get("keypoints", [])
        template_data["lines"] = loaded_data.get("lines", [])

        draw_keypoints_and_lines_loads1(box_frame2, template_data, image_width, image_height)
        print(f"Template loaded from {filename}.json")
    except FileNotFoundError:
        print("Template file not found. Please save a template first.")

# def draw_keypoints_and_lines_loads1(box_frame2,template_data):
#     for item_id in box_frame2.find_withtag("keypoints"):
#         box_frame2.delete(item_id)
#     for item_id in box_frame2.find_withtag("lines"):
#         box_frame2.delete(item_id)
 
#     for x, y in template_data["keypoints"]:
#         box_frame2.create_oval(x - 5, y - 5, x + 5, y + 5, outline="red", fill="red", tags="keypoints")
#     for line in template_data["lines"]:
#         x1, y1 = line[0]
#         x2, y2 = line[1]
#         box_frame2.create_line(x1, y1, x2, y2, fill="blue", width=2, tags="lines")

def draw_keypoints_and_lines_loads1(box_frame2, template_data, image_width, image_height):
    for item_id in box_frame2.find_withtag("keypoints"):
        box_frame2.delete(item_id)
    for item_id in box_frame2.find_withtag("lines"):
        box_frame2.delete(item_id)

    # Get the scaling factors for width and height
    width_scale = box_frame2.winfo_width() / image_width
    height_scale = box_frame2.winfo_height() / image_height

    for x, y in template_data["keypoints"]:
        # Scale the keypoints to fit the displayed image size
        scaled_x = x * width_scale
        scaled_y = y * height_scale

        # Calculate the coordinates to center the keypoints
        x_coordinate = (box_frame2.winfo_width() - image_width) // 2 + scaled_x
        y_coordinate = (box_frame2.winfo_height() - image_height) // 2 + scaled_y

        box_frame2.create_oval(x_coordinate - 5, y_coordinate - 5, x_coordinate + 5, y_coordinate + 5, outline="red",
                                fill="red", tags="keypoints")

    for line in template_data["lines"]:
        x1, y1 = line[0]
        x2, y2 = line[1]

        # Scale the line coordinates to fit the displayed image size
        scaled_x1 = x1 * width_scale
        scaled_y1 = y1 * height_scale
        scaled_x2 = x2 * width_scale
        scaled_y2 = y2 * height_scale

        # Calculate the coordinates to center the lines
        x1_coordinate = (box_frame2.winfo_width() - image_width) // 2 + scaled_x1
        y1_coordinate = (box_frame2.winfo_height() - image_height) // 2 + scaled_y1
        x2_coordinate = (box_frame2.winfo_width() - image_width) // 2 + scaled_x2
        y2_coordinate = (box_frame2.winfo_height() - image_height) // 2 + scaled_y2

        box_frame2.create_line(x1_coordinate, y1_coordinate, x2_coordinate, y2_coordinate, fill="blue", width=2,
                               tags="lines")


def on_keypoint_dragged(event, box_frame2, template_data,image_width, image_height):
    pdb.set_trace()

    drag_data = keypoint_manager.drag_data
    if drag_data:
        delta_x = event.x - drag_data["x"]
        delta_y = event.y - drag_data["y"]

        new_x = drag_data["x"] + delta_x
        new_y = drag_data["y"] + delta_y
        collision = False
        for x, y in template_data["keypoints"]:
                if (new_x - x)**2 + (new_y - y)**2 < 1:  # Adjust this value of Collisionn
                    collision = True
                    break
        if not collision:
            for i, (x, y) in enumerate(template_data["keypoints"]):
                if (x, y) == (drag_data["x"], drag_data["y"]):
                    template_data["keypoints"][i] = (x + delta_x, y + delta_y)
                      
            for i, line in enumerate(template_data["lines"]):
                for j, (x, y) in enumerate(line):
                    if (x, y) == (drag_data["x"], drag_data["y"]):
                        template_data["lines"][i][j] = (x + delta_x, y + delta_y)
            
            draw_keypoints_and_lines_loads1(box_frame2, template_data, image_width, image_height)
            drag_data["x"] = event.x
            drag_data["y"] = event.y


def on_keypoint_clicked(event, box_frame2, template_data):
    x_clicked, y_clicked = event.x, event.y
    radius = 10 
    nearby_keypoints = [(x, y) for x, y in template_data["keypoints"]
                        if (x - x_clicked)**2 + (y - y_clicked)**2 <= radius**2]

    if nearby_keypoints:
        nearest_keypoint = max(nearby_keypoints, key=lambda point: point[1])
        keypoint_manager.drag_data["x"] = nearest_keypoint[0]
        keypoint_manager.drag_data["y"] = nearest_keypoint[1]

import customtkinter as tk
import customtkinter
import json
from tkinter.simpledialog import askstring
import os
from tkinter import messagebox
GRID_SIZE = 25
cuskey_opened = False
class KeypointManager:
    def __init__(self):
        self.keypoints = []
        self.drag_data = {"x": 0, "y": 0}  # Initial values, you can adjust them as needed

keypoint_manager = KeypointManager()
def draw_grids1():
        for x in range(0, canvas.winfo_reqwidth(), GRID_SIZE):
            canvas.create_line(x, 0, x, canvas.winfo_reqheight(), fill="gray", dash=(2, 2))
        for y in range(0, canvas.winfo_reqheight(), GRID_SIZE):
            canvas.create_line(0, y, canvas.winfo_reqwidth(), y, fill="gray", dash=(2, 2))

checked_items = []
def on_clickas1(event):
    if mode == "start":
        x, y = event.x, event.y
        template_data["keypoints"].append((x, y))
        draw_keypoints_and_linesss1()

        # Update the coordinates labels
        xlabel.configure(text=str(x))
        ylabel.configure(text=str(y))
        z = len(template_data["keypoints"])
        zlabel.configure(text=str(z))

        # Add the keypoint name to the checkbox list
        checkbox_text = f"Keypoint {z}"


def draw_keypoints_and_linesss1():
        canvas.delete("Keypoints", "lines")
        draw_grids1()
        for x, y in template_data["keypoints"]:
            canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="red", fill="red")
        if len(template_data["keypoints"]) > 1:
            for i in range(len(template_data["keypoints"]) - 1):
                x1, y1 = template_data["keypoints"][i]
                x2, y2 = template_data["keypoints"][i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
        connectkeypoints()

def drawkeypointsand_linesload():
        canvas.delete("keypoints","lines")
        draw_grids1()
        for x, y in template_data["keypoints"]:
            canvas.create_oval(x - 5, y - 5, x + 5, y + 5, outline="red", fill="red")
        for line in template_data["lines"]:
            x1, y1 = line[0]
            x2, y2 = line[1]
            canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)

def connectkeypoints():
        keypoints = template_data["keypoints"]
        if len(keypoints) > 1:
            lines = []
            for i in range(len(keypoints) - 1):
                x1, y1 = keypoints[i]
                x2, y2 = keypoints[i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
                lines.append([(x1, y1), (x2, y2)])
            template_data["lines"] = lines

def connectkeypoints_all():
        keypoints = template_data["keypoints"]
        if len(keypoints) > 1:
            lines = []
            for i in range(len(keypoints) - 1):
                x1, y1 = keypoints[i]
                x2, y2 = keypoints[i + 1]
                canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
                lines.append([(x1, y1), (x2, y2)])
            # Connect the last and first keypoints
            x1, y1 = keypoints[-1]
            x2, y2 = keypoints[0]
            canvas.create_line(x1, y1, x2, y2, fill="blue", width=2)
            lines.append([(x1, y1), (x2, y2)])
            template_data["lines"] = lines

def undo_lastkeypoint():
        if len(template_data["keypoints"]) > 0:
            template_data["keypoints"].pop()
            draw_keypoints_and_linesss1()

def startmode():
        global mode
        mode = "start"

def finishmode():
        global mode
        mode = "finish"
def finishmode1():
        global mode
        mode = "finish"
def cleartemplate():
        template_data["keypoints"] = []
        template_data["lines"]
        canvas.delete("keypoints","lines")
        draw_grids1()
        xlabel.configure(text=str(0000))
        ylabel.configure(text=str(0000))
        zlabel.configure(text=str(0000))
# def savetemplate():
#     filename = askstring("Save Template", "Enter a filename:")
#     if filename:
#         data_folder = "data"
#         os.makedirs(data_folder, exist_ok=True)
#         # Extract keypoints and lines from template_data
#         keypoints = template_data["keypoints"]
#         lines = template_data["lines"]
#         # Create a dictionary to store the data
#         save_data = {"keypoints": keypoints, "lines": lines}
#         # Add x, y, z coordinates to the dictionary
#         save_data["coordinates"] = [{"x": x, "y": y, "z": z} for z, (x, y) in enumerate(keypoints, start=1)]
#         # Save the data to the JSON file
#         with open(os.path.join(data_folder, f"{filename}.json"), "w") as json_file:
#             json.dump(save_data, json_file)
#         print(f"Template saved to {filename}.json")

def savetemplate():
    filename = askstring("Save Template", "Enter a filename:")
    if filename:
        data_folder = "data"
        os.makedirs(data_folder, exist_ok=True)
        keypoints = template_data["keypoints"]
        lines = template_data["lines"]
        save_data = {"keypoints": keypoints, "lines": lines}
        save_data["keypoints_names"] = [f"Keypoint {i}" for i in range(1, len(keypoints) + 1)]
        save_data["coordinates"] = [{"x": x, "y": y, "z": z, "name": name} for z, (x, y), name in zip(range(1, len(keypoints) + 1), keypoints, save_data["keypoints_names"])]
        with open(os.path.join(data_folder, f"{filename}.json"), "w") as json_file:
            json.dump(save_data, json_file)
        print(f"Template saved to {filename}.json")

def loadtemplate():
    try:
        filename = askstring("Load Template", "Enter a filename:")
        with open(os.path.join("data", f"{filename}.json"), "r") as json_file:
            loaded_data = json.load(json_file)
        
        template_data["keypoints"] = loaded_data.get("keypoints", [])
        template_data["lines"] = loaded_data.get("lines", [])

        drawkeypointsand_linesload()

        # Extract and display the coordinates
        coordinates = loaded_data.get("coordinates", [])
        if coordinates:
            last_coordinate = coordinates[-1]
            xlabel.configure(text=str(last_coordinate["x"]))
            ylabel.configure(text=str(last_coordinate["y"]))
            zlabel.configure(text=str(last_coordinate["z"]))

        print(f"Template loaded from {filename}.json")
    except FileNotFoundError:
        print("Template file not found. Please save a template first.")



def on_keypoint_clicked(event):
    finishmode1()
    global mode
    mode = "edit"
    x_clicked, y_clicked = event.x, event.y

    # Find all keypoints within a certain radius
    radius = 10  # Adjust this value as needed
    nearby_keypoints = [(x, y) for x, y in keypoint_manager.keypoints
                        if (x - x_clicked)**2 + (y - y_clicked)**2 <= radius**2]

    if nearby_keypoints:
        nearest_keypoint = max(nearby_keypoints, key=lambda point: point[1])
        keypoint_manager.drag_data["x"] = nearest_keypoint[0]
        keypoint_manager.drag_data["y"] = nearest_keypoint[1]
def on_keypoint_dragged(event):
    global mode
    if mode == "edit":
        drag_data = keypoint_manager.drag_data
    drag_data = keypoint_manager.drag_data
    if drag_data:
        delta_x = event.x - drag_data["x"]
        delta_y = event.y - drag_data["y"]

        # Check for collision with other keypoints
        new_x = drag_data["x"] + delta_x
        new_y = drag_data["y"] + delta_y

        collision = False
        for x, y in keypoint_manager.keypoints:
            if (new_x - x)**2 + (new_y - y)**2 < 1:  # Adjust this value as needed
                collision = True
                break

        if not collision:
            for i, (x, y) in enumerate(keypoint_manager.keypoints):
                if (x, y) == (drag_data["x"], drag_data["y"]):
                    keypoint_manager.keypoints[i] = (x + delta_x, y + delta_y)

            for i, line in enumerate(template_data["lines"]):
                for j, (x, y) in enumerate(line):
                    if (x, y) == (drag_data["x"], drag_data["y"]):
                        template_data["lines"][i][j] = (x + delta_x, y + delta_y)

            draw_keypoints_and_linesss1()
            drag_data["x"] = event.x
            drag_data["y"] = event.y
   
########################################## SCROLL FUNCTION ###################################################################################################################################################
def checkbox_frame_event():
    print(f"checkbox frame modified: {get_checked_items()}")
def add_item(item):
    checkbox = customtkinter.CTkCheckBox(scrollable_frame, text=item, command=checkbox_frame_event)
    checkbox.grid(row=len(checkbox_list), column=0, pady=(0, 10))
    checkbox_list.append(checkbox)
def remove_item(item):
    for checkbox in checkbox_list:
        if item == checkbox.cget("text"):
            checkbox.destroy()
            checkbox_list.remove(checkbox)
            return
def get_checked_items():
    return [checkbox.cget("text") for checkbox in checkbox_list if checkbox.get() == 1]
###############################################################################################################################################################################################


customtkinter.set_appearance_mode("System")
customtkinter.set_default_color_theme("blue")
root = customtkinter.CTk()
root.geometry("920x560")
root.title("Keypoints Labeling")




canvas = tk.CTkCanvas(root, width=560, height=480, bg="white",highlightbackground="black", highlightthickness=1)
canvas.place(x=5,y=5)

template_data = {"keypoints": [], "lines": []}
mode = "finish"
box1 = tk.CTkFrame(root, width=300, height=150)
box1.place(x=600,y=5)
tittleLabel = tk.CTkLabel(box1, width=280, height=25,text="Coordinates", bg_color="transparent", fg_color="green",text_color="white", corner_radius=10)
tittleLabel.place(x=10,y=10)

xCoordinate = tk.CTkLabel(box1,text="X: ")
xCoordinate.place(x=100,y=40)
xlabel = tk.CTkLabel(box1,text="0000")
xlabel.place(x=140,y=40)

yCoordinate = tk.CTkLabel(box1,text="Y: ")
yCoordinate.place(x=100,y=70)
ylabel = tk.CTkLabel(box1,text="0000")
ylabel.place(x=140,y=70)

zCoordinate = tk.CTkLabel(box1,text="Z: ")
zCoordinate.place(x=100,y=100)
zlabel = tk.CTkLabel(box1,text="0000")
zlabel.place(x=140,y=100)

box2 = tk.CTkFrame(root, width=300, height=370)
box2.place(x=600,y=170)


box2_label_names = tk.CTkLabel(box2, width=280, height=25, text="Keypoints", bg_color="transparent", fg_color="green", text_color="white", corner_radius=10)
box2_label_names.grid(row=0, column=0, pady=(5, 10), padx=15, sticky="ns")

# create scrollable checkbox frame
scrollable_frame = customtkinter.CTkScrollableFrame(box2, width=200)
scrollable_frame.grid(row=1, column=0, padx=15, pady=15, sticky="ns")

checkbox_list = []
for i in range(50):
    add_item(f"item {i}")

add_item("new item")

##############################################################################################################################################################################3


start_button = tk.CTkButton(root, text="Start", command=startmode)
start_button.place(x=10,y=490)
finish_button = tk.CTkButton(root, text="Finish", command=finishmode)
finish_button.place(x=10,y=525)

connect_button = tk.CTkButton(root, text="Connect", command=connectkeypoints_all)
connect_button.place(x=160,y=490)

undo_button = tk.CTkButton(root, text="Undo", command=on_keypoint_clicked)
undo_button.place(x=160,y=525)

edit_button = tk.CTkButton(root, text="Edit", command=finishmode1)
edit_button.place(x=310,y=490)

clear_button = tk.CTkButton(root, text="Clear", command=cleartemplate)
clear_button.place(x=310,y=525)

save_button = tk.CTkButton(root, text="Save", command=savetemplate)
save_button.place(x=460,y=490)

load_button = tk.CTkButton(root, text="Load", command=loadtemplate)
load_button.place(x=460,y=525)

canvas.bind("<Button-1>", on_clickas1)
canvas.bind("<B1-Motion>",  on_keypoint_dragged)

draw_grids1()
root.mainloop()


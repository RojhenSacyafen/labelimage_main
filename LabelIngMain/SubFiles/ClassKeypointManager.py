class KeypointManager:
    def __init__(self):
        self.template_data = {"keypoints": [], "lines": [], "keypoint_counter":0, "selected_keypoints": []}
        self.drag_data = {"x": 0, "y": 0}
        self.undo_stack = []  
        self.max_undo_steps = 10 
        # self.keypoints = []
        self.selected_keypoint_index = None
        self.select_key = {"x": 0, "y": 0}
keypoint_manager = KeypointManager()
import os
from tkinter.simpledialog import askstring
import json

def draw_keypoints_and_lines_loads1(box_frame2, template_data):
        box_frame2.delete("all")
        # draw_grid()
        for x, y in template_data["keypoints"]:
            box_frame2.create_oval(x - 5, y - 5, x + 5, y + 5, outline="red", fill="red")
        for line in template_data["lines"]:
            x1, y1 = line[0]
            x2, y2 = line[1]
            box_frame2.create_line(x1, y1, x2, y2, fill="blue", width=2)
def load_templates1(box_frame2, template_data):
        try:
            filename = askstring("Load Template", "Enter a filename:")
            with open(os.path.join("data", f"{filename}.json"), "r") as json_file:
                loaded_data = json.load(json_file)
            template_data["keypoints"] = loaded_data.get("keypoints", [])
            template_data["lines"] = loaded_data.get("lines", [])

            draw_keypoints_and_lines_loads1(box_frame2,template_data)
            print(f"Template loaded from {filename}.json")
        except FileNotFoundError:
                print("Template file not found. Please save a template first.")
        except json.JSONDecodeError:
                print("Error decoding JSON. Please check your template file.")
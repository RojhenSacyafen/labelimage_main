from tkinter.simpledialog import askstring
import os
import json
from PIL import Image, ImageDraw

# COUNTER_FILE = "DataSet/Annotate"
# def get_counter():
#     if not os.path.exists(COUNTER_FILE):
#         with open(COUNTER_FILE, "w") as file:
#             file.write("0")
#     with open(COUNTER_FILE, "r") as file:
#         counter = int(file.read())
#     return counter
# def update_counter(counter):
#     with open(COUNTER_FILE, "w") as file:
#         file.write(str(counter + 1))
data_folder = "DataSet/Annotate"

def count_files_in_folder(folder_path):
    if not os.path.exists(folder_path) or not os.path.isdir(folder_path):
        return 0
    return len(os.listdir(folder_path))

number_of_files = count_files_in_folder(data_folder)


def normalize_coordinates(x, y, width, height):
    normalized_x = x / width
    normalized_y = y / height
    return normalized_x, normalized_y

def savetemplate(template_data, space_width, space_height):
    global number_of_files
    filename = f"template_{number_of_files}"
  

    data_folder = "DataSet/Annotate"
    os.makedirs(data_folder, exist_ok=True)
    data_folder_annotate_img = "DataSet/Annotate_img"
    os.makedirs(data_folder_annotate_img, exist_ok=True)
    keypoints = template_data["keypoints"]
    lines = template_data["lines"]

    if not keypoints:
        print("Error: No keypoints found. Cannot calculate bounding box.")
        return
    
    # Calculate bounding box based on the keypoints
    min_x = min(keypoints, key=lambda p: p[0])[0]
    min_y = min(keypoints, key=lambda p: p[1])[1]
    max_x = max(keypoints, key=lambda p: p[0])[0]
    max_y = max(keypoints, key=lambda p: p[1])[1]
    normalized_min_x, normalized_min_y = normalize_coordinates(min_x, min_y, space_width, space_height)
    normalized_max_x, normalized_max_y = normalize_coordinates(max_x, max_y, space_width, space_height)
    bounding_box = [normalized_min_x, normalized_min_y, normalized_max_x, normalized_max_y]
    normalized_keypoints = [coord for point in keypoints for coord in normalize_coordinates(point[0], point[1], space_width, space_height)]
    save_data = {
        "label": [],
        "bounding_box": bounding_box,
        "key_points": normalized_keypoints
    }
    with open(os.path.join(data_folder, f"{filename}.json"), "w") as json_file:
        json.dump([save_data], json_file, indent=4) 
    number_of_files += 1 
    image_width, image_height = int(space_width), int(space_height)
    image = Image.new("RGB", (image_width, image_height), "white")
    draw = ImageDraw.Draw(image)
    for line in lines:
        line_int = [(int(x), int(y)) for x, y in line]
        draw.line(line_int, fill="blue", width=2)
    box_coordinates = [
        (normalized_min_x * space_width, normalized_min_y * space_height),
        (normalized_max_x * space_width, normalized_min_y * space_height),
        (normalized_max_x * space_width, normalized_max_y * space_height),
        (normalized_min_x * space_width, normalized_max_y * space_height),
        (normalized_min_x * space_width, normalized_min_y * space_height)
    ]
    draw.polygon(box_coordinates, outline="red")
    for point in keypoints:
        x, y = normalize_coordinates(point[0], point[1], space_width, space_height)
        draw.ellipse([x * space_width - 3, y * space_height - 3, x * space_width + 3, y * space_height + 3], fill="blue")
    image.save(os.path.join(data_folder_annotate_img, f"{filename}.png"))

    print(f"Display image saved to {filename}.png")
    print(f"Template saved to {filename}.json")

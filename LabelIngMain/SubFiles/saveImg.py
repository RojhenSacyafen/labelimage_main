import os
from PIL import Image
from SubFiles.ClassPath import data_folder
data_folder=data_folder.data_folders
# data_folder = "DataSet/Images"
def count_files_in_folder(folder_path):
    if not os.path.exists(folder_path) or not os.path.isdir(folder_path):
        return 0
    return len(os.listdir(folder_path))

number_of_files = count_files_in_folder(data_folder)


def save_image(image_path):
    global number_of_files
    image = Image.open(image_path)
    save_path = os.path.join(data_folder, f"image_{number_of_files}.png")
    image.save(save_path)
    number_of_files += 1
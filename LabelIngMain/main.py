import customtkinter
from PIL import Image, ImageTk
import os
import cv2
import numpy as np
import json
from tkinter.simpledialog import askstring
from SubFiles.loadimage import open_and_load_images1,save_loaded_image
# from Files.LoadTemplates import load_templates1
from SubFiles.clearcanva import clear_template, on_button_click_clears1
from SubFiles.loadtemplatesample import on_keypoint_clicked, on_keypoint_dragged, load_templates1
from SubFiles.ClassKeypointManager import keypoint_manager
from Keypoint_files.CustomKeys2 import CusKey
from SubFiles.saveAnnotate import savetemplate
# from SubFiles.saveImg import save_image




customtkinter.set_appearance_mode("System")
customtkinter.set_default_color_theme("blue")
app = customtkinter.CTk()
app.geometry("1360x960")
app.title("Label Image")
tittle = customtkinter.CTkLabel(app, text="HERO YUI")
tittle.pack(padx=10,pady=10)

template_data = keypoint_manager.template_data


###################################################   Box Frame 1  ######################################################################################3###############################################3
box_frame1 = customtkinter.CTkFrame(app,width=200, height=1000, bg_color="gray", corner_radius=10)
box_frame1.pack(side="left", padx=5, pady=5, fill="both", expand=True)
box_frame1.pack_propagate(False)
#box_frame.place(x=100, y=200)

button_image_folder = customtkinter.CTkImage(Image.open("Img/folder.png"), size=(26, 26))
button_folder = customtkinter.CTkButton(box_frame1,   width=100, height=50, text="Open Image", image=button_image_folder,compound="top",command=lambda: open_and_load_images1(box_frame2))
button_folder.pack(padx=5, pady=5)

button_image_next = customtkinter.CTkImage(Image.open("Img/next-button.png"), size=(26, 26))
button_next = customtkinter.CTkButton(box_frame1,   width=100, height=50, text="Open Image", image=button_image_next,compound="top")
button_next.pack(padx=5, pady=5)

button_image_previous = customtkinter.CTkImage(Image.open("Img/previous.png"), size=(26, 26))
button_previous = customtkinter.CTkButton(box_frame1,  width=100, height=50, text="Previous Image", image=button_image_previous, compound="top")
button_previous.pack(padx=5, pady=5)

button_image_clear = customtkinter.CTkImage(Image.open("Img/archeology.png"), size=(26, 26))
button_clear = customtkinter.CTkButton(box_frame1,  width=100, height=50, text="Clear", image=button_image_clear, compound="top",command=lambda: on_button_click_clears1(box_frame2))
button_clear.pack(padx=5, pady=5)
###########################################  Box Frame 2  ######################################################################################################################################3

box_frame2 = customtkinter.CTkCanvas(app,width=900, height=1000)
box_frame2.pack(side="left", padx=5, pady=5, fill="both", expand=True)
box_frame2.bind("<B1-Motion>", lambda event: on_keypoint_dragged(event, box_frame2, template_data, box_frame2.winfo_width(), box_frame2.winfo_height()))
box_frame2.bind("<Button-1>", lambda event: on_keypoint_clicked(event, box_frame2, template_data))
box_frame2.pack_propagate(False)


# box_frame2_canvas = customtkinter.CTkCanvas(box_frame2,width=900, height=1000)
# box_frame2_canvas.pack(side="left", padx=5, pady=5, fill="both", expand=True)
# box_frame2_canvas.bind("<B1-Motion>", lambda event: on_keypoint_dragged(event, box_frame2_canvas, keypoint_manager,))
# box_frame2_canvas.bind("<Button-1>", lambda event: on_keypoint_clicked(event, box_frame2_canvas, keypoint_manager))
# box_frame2_canvas.pack_propagate(False)

# box_frame2_canva2 = customtkinter.CTkCanvas(app,width=900, height=1000,background=)
# box_frame2_canva2.pack(side="left", padx=5, pady=5, fill="both", expand=True,)
# box_frame2_canva2.bind("<B1-Motion>", lambda event: on_keypoint_dragged(event, box_frame2, keypoint))
# box_frame2_canva2.bind("<Button-1>", lambda event: on_keypoint_clicked(event, box_frame2, keypoint))
# box_frame2_canva2.pack_propagate(False)

###########################################  Box Frame 3  #########################################################################################################################################33

box_frame3 = customtkinter.CTkFrame(app, width=200, height=1000,border_width=2,border_color="black",bg_color="gray", corner_radius=10)
box_frame3.pack(side="left", padx=5, pady=5, fill="both", expand=True)
box_frame3.pack_propagate(False)

def show_frame(frame_to_show):
    for frame in [box_frame2_inside1, box_frame2_inside2, box_frame2_inside3]:
        if frame == frame_to_show:
            frame.grid(row=5, column=1, columnspan=2, padx=5, pady=5, sticky="nsew")
            frame.pack_propagate(False)
        else:
            frame.grid_remove()

def on_option_selected(value):
    if value == "Annotate":
        show_frame(box_frame2_inside1)
    elif value == "Keypoints":
        show_frame(box_frame2_inside2)
    elif value == "sample":
        show_frame(box_frame2_inside3)

select_button = customtkinter.CTkOptionMenu(box_frame3, values=["Annotate", "Keypoints", "sample"],command=on_option_selected)
select_button.grid(row=4, column=1, columnspan=2, padx=20, pady=20, sticky="ew")

box_frame2_inside1 = customtkinter.CTkFrame(box_frame3, width=200, height=400,border_width=1,border_color="black", bg_color="green", corner_radius=10)
box_frame2_inside1.pack_propagate(False)
box_frame2_inside2 = customtkinter.CTkFrame(box_frame3, width=200, height=400,border_width=1,border_color="black", bg_color="blue", corner_radius=10)
box_frame2_inside2.pack_propagate(False)
box_frame2_inside3 = customtkinter.CTkFrame(box_frame3, width=200, height=400,border_width=1,border_color="black", bg_color="red", corner_radius=10)
box_frame2_inside3.pack_propagate(False)


for frame in [box_frame2_inside1, box_frame2_inside2, box_frame2_inside3]:
    frame.grid_remove()
show_frame(box_frame2_inside1)

box_frame2_inside1_box = customtkinter.CTkFrame(box_frame2_inside1, width=50,height=390,fg_color="green",corner_radius=10)
box_frame2_inside1_box.pack(side="left",padx=5,pady=5)
box_frame2_inside1_box.propagate(False)
box_frame2_inside1_box2 = customtkinter.CTkFrame(box_frame2_inside1, width=150,height=390,fg_color="dark gray",corner_radius=10)
box_frame2_inside1_box2.pack(side="left",padx=5,pady=2)
box_frame2_inside1_box2.propagate(False)

box_frame2_inside1_box_button_drag_image = customtkinter.CTkImage(Image.open("Img/drag.png"), size=(26, 26))
box_frame2_inside1_box_button_Drag = customtkinter.CTkButton(box_frame2_inside1_box,image=box_frame2_inside1_box_button_drag_image, width=20, height=20, text=None, compound="top", fg_color="white",corner_radius=5)
box_frame2_inside1_box_button_Drag.pack(padx=2,pady=5)

box_frame2_inside1_box_button_animation_image = customtkinter.CTkImage(Image.open("Img/animation.png"), size=(26, 26))
box_frame2_inside1_box_button_Animation = customtkinter.CTkButton(box_frame2_inside1_box, image = box_frame2_inside1_box_button_animation_image, width=20, height=20, text=None, 
                                                                  compound="top", fg_color="white",corner_radius=5,command=lambda: load_templates1(box_frame2, template_data, box_frame2.winfo_width(), box_frame2.winfo_height()))
box_frame2_inside1_box_button_Animation.pack(padx=2,pady=5)

box_frame2_inside1_box_button_save_image = customtkinter.CTkImage(Image.open("Img/save.gif"), size=(26, 26))
box_frame2_inside1_box_button_save = customtkinter.CTkButton(box_frame2_inside1_box, image=box_frame2_inside1_box_button_save_image, width=20, height=20, text=None, 
                                                              compound="top", fg_color="white",corner_radius=5,
                                                              command=lambda : [savetemplate(template_data, space_width=560, space_height=480),save_loaded_image()])
box_frame2_inside1_box_button_save.pack(padx=2,pady=5)

box_frame2_inside1_box_button_trash_image = customtkinter.CTkImage(Image.open("Img/trash.png"), size=(26, 26))
box_frame2_inside1_box_button_trash = customtkinter.CTkButton(box_frame2_inside1_box, image=box_frame2_inside1_box_button_trash_image, width=20, height=20, text=None, 
                                                              compound="top", fg_color="white",corner_radius=5,command=lambda: clear_template(box_frame2))
box_frame2_inside1_box_button_trash.pack(padx=2,pady=5)


box_frame2_inside2_CustomKeypoints = customtkinter.CTkButton(box_frame2_inside2, width=190,height=10,text="Custom Keypoints", command=CusKey)
box_frame2_inside2_CustomKeypoints.pack(padx=5, pady=5)

bounding_box = customtkinter.CTkFrame(box_frame3, width=200, height=400,border_width=1,border_color="black", bg_color="green", corner_radius=10)
bounding_box.grid(row=5, column=1, columnspan=2, padx=20, pady=20, sticky="nsew")
bounding_box.place(x=5,y=500)

bounding_box_label = customtkinter.CTkLabel(bounding_box,width=190, height=30,text="Bounding Box",bg_color="green")
bounding_box_label.place(x=5,y=5)

bounding_box_label_x_min = customtkinter.CTkLabel(bounding_box,text="x_min: ")
bounding_box_label_x_min.place(x=15,y=55)

bounding_box_label_y_min = customtkinter.CTkLabel(bounding_box,text="y_min: ")
bounding_box_label_y_min.place(x=15,y=100)

bounding_box_label_x_max = customtkinter.CTkLabel(bounding_box,text="x_max: ")
bounding_box_label_x_max.place(x=15,y=150)

bounding_box_label_y_max = customtkinter.CTkLabel(bounding_box,text="y_max: ")
bounding_box_label_y_max.place(x=15,y=200)

bounding_box_label_x_min1 = customtkinter.CTkLabel(bounding_box,text="0000")
bounding_box_label_x_min1.place(x=105,y=55)

bounding_box_label_y_min1 = customtkinter.CTkLabel(bounding_box,text="0000")
bounding_box_label_y_min1.place(x=105,y=100)

bounding_box_label_x_max1 = customtkinter.CTkLabel(bounding_box,text="0000")
bounding_box_label_x_max1.place(x=105,y=150)

bounding_box_label_y_max1 = customtkinter.CTkLabel(bounding_box,text="0000")
bounding_box_label_y_max1.place(x=105,y=200)



app.mainloop()
